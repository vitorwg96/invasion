using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

public class CardManager : MonoBehaviour
{
    public event UnityAction<int> ReduceEnergy;

    [SerializeField] private Camera _mainCamera;
    [SerializeField] private UIManager _uiManager;

    [SerializeField] private Card _card;
    [SerializeField] private LayerMask _playingFieldMask;
    [SerializeField] private GameObject[] _prefabs;

    [SerializeField] private Card[] _cards;
    private GameObject _previewHolder;
    private bool cardIsActive = false;


    private void Awake()
    {
        _previewHolder = new GameObject("PreviewHolder");
    }

    private void OnEnable()
    {
        for (int i = 0; i < _cards.Length; i++)
        {
            _cards[i].OnTapDownAction += CardTapped;
            _cards[i].OnDragAction += CardDragged;
            _cards[i].OnTapReleaseAction += CardReleased;
        }

    }

    private void CardTapped(CardDataSO.CardType cardType)
    {
        _cards[(int)cardType].GetComponent<RectTransform>().SetAsFirstSibling();
    }

    // condition have enough energy
    private void CardDragged(CardDataSO.CardType cardType, Vector2 dragAmount)
    {
        _cards[(int)cardType].transform.Translate(dragAmount);

        RaycastHit hit;
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

        bool planeHit = Physics.Raycast(ray, out hit, Mathf.Infinity, _playingFieldMask);

        if (planeHit)
        {
            //Debug.Log("Is Hiting: " + hit.collider.name);

            if (!cardIsActive)
            {
                cardIsActive = true;
                _previewHolder.transform.position = hit.point;
                _cards[(int)cardType].ChangeActiveState(true);

                GameObject newPlaceable = GameObject.Instantiate<GameObject>(_prefabs[(int)cardType], hit.point, Quaternion.identity, _previewHolder.transform);
            } 
            else
            {
                _previewHolder.transform.position = hit.point;
            }

        }
        else
        {
            if (cardIsActive)
            {
                cardIsActive = false;
                _cards[(int)cardType].ChangeActiveState(false);

                ClearPreviewObjects();
            }
        }

    }

    private void CardReleased(CardDataSO.CardType cardType)
    {
        RaycastHit hit;
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

        bool hasEnergy = _uiManager.GetCurrentEnergy() >= _card._cardData.energyCost;
        //print(hasEnergy);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _playingFieldMask) && hasEnergy)
        {
            //Debug.Log("CardReleased: " + hit.collider.name);
            GameObject newPlaceableGO = Instantiate<GameObject>(_prefabs[(int)cardType], hit.collider.transform.position, Quaternion.identity);
            ReduceEnergy?.Invoke(_cards[(int)cardType]._cardData.energyCost);
            ClearPreviewObjects();
            _cards[(int)cardType].GetComponent<RectTransform>().DOAnchorPos(_cards[(int)cardType]._rectTransform, 0.2f).SetEase(Ease.OutQuad);
            _cards[(int)cardType].ChangeActiveState(false);
        }
        else
        {
            _cards[(int)cardType].GetComponent<RectTransform>().DOAnchorPos(_cards[(int)cardType]._rectTransform, 0.2f).SetEase(Ease.OutQuad);
        }
    }

    private void ClearPreviewObjects()
    {
        for (int i = 0; i < _previewHolder.transform.childCount; i++)
        {
            Destroy(_previewHolder.transform.GetChild(i).gameObject);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < _cards.Length; i++)
        {
            _cards[i].OnTapDownAction -= CardTapped;
            _cards[i].OnDragAction -= CardDragged;
            _cards[i].OnTapReleaseAction -= CardReleased;
        }
    }

}
