using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class InputReader : MonoBehaviour, GameInput.IGameplayActions
{
    public event UnityAction LeftClickEvent;
    public event UnityAction LeftClickCanceledEvent;
    public event UnityAction RightClickEvent;
    public event UnityAction RightClickCanceledEvent;


    private GameInput _gameInput;

    private void OnEnable()
    {
        if(_gameInput == null)
        {
            _gameInput = new GameInput();

            _gameInput.Gameplay.SetCallbacks(this);
        }

        EnableGameplayInput();
    }

    private void OnDisable()
    {
        DisableAllInput();
    }

    public void OnLeftMouseButton(InputAction.CallbackContext context)
    {

        switch (context.phase)
        {
            case InputActionPhase.Performed:
                //Debug.Log("LeftMouseButton: " + context.phase);
                LeftClickEvent?.Invoke();
                break;
            case InputActionPhase.Canceled:
                //Debug.Log("LeftMouseButton: " + context.phase);
                LeftClickCanceledEvent?.Invoke();
                break;
        }
    }

    public void OnRightMouseButton(InputAction.CallbackContext context)
    {
        switch (context.phase)
        {
            case InputActionPhase.Performed:
                //Debug.Log("LeftMouseButton: " + context.phase);
                RightClickEvent?.Invoke();
                break;
            case InputActionPhase.Canceled:
                //Debug.Log("LeftMouseButton: " + context.phase);
                RightClickCanceledEvent?.Invoke();
                break;
        }
    }

    public void EnableGameplayInput()
    {
        _gameInput.Gameplay.Enable();
    }

    public void DisableAllInput()
    {
        _gameInput.Gameplay.Disable();
    }
}
