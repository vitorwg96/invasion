using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Transform[] _spawnPoints;
    [SerializeField] private GameObject[] _enemyPrefabs;
    [SerializeField] private float _respawnTimer = 5f;

    private void Start()
    {
        SpawnEnemy();
    }

    private void SpawnEnemy()
    {
        GameObject randomEnemy = RandomEnemyPrefabGenerator();
        Instantiate(randomEnemy, RandomPointGenerator(), Quaternion.AngleAxis(-90f, Vector3.up));
        StartCoroutine(TimeToRespawn(_respawnTimer));

    }

    private IEnumerator TimeToRespawn(float time)
    {
        yield return new WaitForSeconds(time);
        _respawnTimer -= 0.2f;
        SpawnEnemy();
    }

    private Vector3 RandomPointGenerator()
    {
        int i = Random.Range(0, _spawnPoints.Length);

        return _spawnPoints[i].position;
    }

    private GameObject RandomEnemyPrefabGenerator()
    {
        int i = Random.Range(0, _enemyPrefabs.Length);
 
        return _enemyPrefabs[i];
    }

}
