using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLife : PlaceableLife
{
    private Animator _animator;
    private CapsuleCollider _capsuleCollider;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _capsuleCollider = GetComponent<CapsuleCollider>();
    }


    public override void OnDied()
    {
        base.OnDied();
        _capsuleCollider.enabled = false;
        _animator.SetTrigger("isDead");
        Destroy(gameObject, 2f);
    }

}
