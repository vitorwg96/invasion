using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GeneratorResource : MonoBehaviour
{
    public event UnityAction<int> AddEnergy;

    [SerializeField] private PlaceableDataSO _placeableData;
    [SerializeField] private int _energyAmount;

    private void Start()
    {
        StartCoroutine(GenerateEnergy());
    }

    private IEnumerator GenerateEnergy()
    {
        Debug.Log("Add energy: " + _energyAmount + " in " + _placeableData.MecanicCooldown + " seconds");
        yield return new WaitForSeconds(2f);
        AddEnergy?.Invoke(_energyAmount);
        StartCoroutine(GenerateEnergy());
    }
}
