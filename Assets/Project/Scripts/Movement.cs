using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Movement : MonoBehaviour
{
    public event UnityAction<GameObject> IsColliding;

    [SerializeField] private PlaceableDataSO _placeableDataSO;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        //_rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        Move();
    }

    private void Update()
    {
        //transform.position += Vector3.left * _placeableDataSO.MoveSpeed * Time.deltaTime;
        //Debug.Log(_rigidbody.velocity);
    }

    public void Move()
    {
        //_rigidbody.velocity = Vector3.left * _placeableDataSO.MoveSpeed;
    }

    public void Stop()
    {
        _rigidbody.velocity = Vector3.zero;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.TryGetComponent(out TowerLife towerLife))
        {
            IsColliding?.Invoke(collision.gameObject);
           // event on collider
            
        }
    }

}
