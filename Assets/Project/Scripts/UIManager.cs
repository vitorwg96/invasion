using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GeneratorResource _generatorResource;
    [SerializeField] private CardManager _cardManager;
    [SerializeField] private TextMeshProUGUI _energy;
    [SerializeField] private int _initializeEnergy = 100;

    private int _currentEnergy;

    private void Start()
    {
        _currentEnergy = _initializeEnergy;
        _energy.text = "Energy: " + _currentEnergy.ToString();
    }

    private void OnEnable()
    {
        _generatorResource.AddEnergy += AddCurrentEnergy;
        _cardManager.ReduceEnergy += ReduceCurrentEnergy;
    }

    private void ReduceCurrentEnergy(int energyAmount)
    {
        _currentEnergy -= energyAmount;
        _energy.text = "Energy: " + _currentEnergy.ToString();
    }

    private void AddCurrentEnergy(int energyAmount)
    {
        Debug.Log("Here");
        _currentEnergy += energyAmount;
        _energy.text = "Energy: " + _currentEnergy.ToString();
    }

    public int GetCurrentEnergy()
    {
        return _currentEnergy;
    }

    private void OnDisable()
    {
        _generatorResource.AddEnergy -= AddCurrentEnergy;
        _cardManager.ReduceEnergy -= ReduceCurrentEnergy;
    }
}
