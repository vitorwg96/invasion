using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class Card : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    public UnityAction<CardDataSO.CardType, Vector2> OnDragAction;
    public UnityAction<CardDataSO.CardType> OnTapDownAction, OnTapReleaseAction;

    public CardDataSO _cardData;

    [HideInInspector] public int _cardId;

    [SerializeField] private TextMeshProUGUI _energy;
    public Vector2 _rectTransform;

    private CanvasGroup _canvasGroup;

    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        _energy.text = _cardData.energyCost.ToString();
    }

    public void OnPointerDown(PointerEventData pointerEvent)
    {
        if (OnTapDownAction != null)
        {
            OnTapDownAction(_cardData.type);
        }
    }

    public void OnDrag(PointerEventData pointerEvent)
    {
        if (OnDragAction != null)
        {
            OnDragAction(_cardData.type, pointerEvent.delta);
        }
    }

    public void OnPointerUp(PointerEventData pointerEvent)
    {
        if (OnTapReleaseAction != null)
        {
            OnTapReleaseAction(_cardData.type);
        }
    }

    /// <summary>
    /// Set Alpha paramenter of the card (hide card)
    /// </summary>
    /// <param name="isActive"></param>
    public void ChangeActiveState(bool isActive)
    {
        _canvasGroup.alpha = (isActive) ? .01f : 1f;
    }

}
