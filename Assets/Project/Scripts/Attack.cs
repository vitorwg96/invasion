using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    // subscribe a event when enemy coolider with a tower
    // when collider start attack this obj
    // when the tower die callback for event continue movement

    // outra alternativa � ele n�o parar de andar, quando colidir inicia a canima��o em loop e da dano pela animationevent

    [SerializeField] private Movement _movementScript;
    [SerializeField] private PlaceableDataSO _placeableData;

    private TowerLife _towerLife;
    private Animator _animator;
    private float _cd;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _movementScript.IsColliding += AttackTrigger;
        
    }
    private void Update()
    {
        //CheckIfCanAttack();
    }


    public void AttackTrigger(GameObject tower)
    {
        _towerLife = tower.GetComponent<TowerLife>();
        _animator.SetBool("isAttacking", true);
        _towerLife.TowerDie += ContinueMove;
        
        //CheckIfCanAttack(tower);
        //StartCoroutine(Cooldown(_placeableData.MecanicCooldown));
        //CheckDistance(tower);
    }

    private void ContinueMove()
    {
        _animator.SetBool("isAttacking", false);
    }

    public void DealDamage()
    {
        _towerLife.TakeDamage(_placeableData.Damage);
    }
}
