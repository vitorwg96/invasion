using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableLife : MonoBehaviour
{
    [SerializeField] protected PlaceableDataSO _unitStats;

    private int _currentHealth;

    private void Start()
    {
        _currentHealth = _unitStats.Health;
    }

    public virtual void TakeDamage(int damage)
    {
        _currentHealth -= damage;

        Debug.Log(gameObject.name + " receive the damage");

        if (_currentHealth <= 0)
        {
            OnDied();
        }
    }

    public virtual void OnDied()
    {
        Debug.Log(gameObject.name + " isDead");
    }
}
