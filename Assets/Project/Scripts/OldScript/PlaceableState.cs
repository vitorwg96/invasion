using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceabldeState : Placeable
{
    public States _state;

    public enum States
    {
        Idle,
        Attacking,
        GeneratingResource,
        Dead,
    }

    public enum AttackType
    {
        Melee,
        Ranged,
    }

    public virtual void Idle()
    {
        _state = States.Idle;
    }

    public virtual void GenerateResource()
    {
        _state = States.GeneratingResource;
    }

    public virtual void Die()
    {
        _state = States.Dead;
    }


}
