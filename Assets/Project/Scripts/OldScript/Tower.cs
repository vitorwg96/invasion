using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : PlaceabldeState
{
    [SerializeField] PlaceableDataSO _placeableData;

    private bool isDead = false;

    private void Update()
    {
        //while (!isDead)
        //{
        //    switch (_state)
        //    {
        //        case PlaceabldeState.States.Idle:
        //            // TryFind a Target on line. using ray
        //            // Find a target change to Attack State
        //            break;

        //        case PlaceabldeState.States.Attacking:
        //            StartCoroutine(SpawnProjectile(5));
        //            break;

        //        case PlaceabldeState.States.GeneratingResource:
        //            StartCoroutine(Generate());
        //            break;

        //        case PlaceabldeState.States.Dead:
        //            // How will call this method?
        //            // Die();
        //            break;
        //    }
        //}


    }

    private void Start()
    {
        StartCoroutine(SpawnProjectile(20));
    }

    private void TryFoundTarget()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.right, out hit, 15))
        {
            // if true
            _state = States.Attacking;
        }
    }

    public IEnumerator SpawnProjectile(int time)
    {
        Vector3 offSet = new Vector3(0f, 7f, 0f);
        GameObject projectile = Instantiate(_placeableData.ObjectPrefab, gameObject.transform.position + offSet, Quaternion.identity);
        projectile.TryGetComponent(out Projectile p);
        //p.Shoot();
        yield return new WaitForSeconds(time);
        Debug.Log("SHoot");

        // Instanciate projectile
        // _projectile.Shot;
    }

    private IEnumerator Generate()
    {
        // Instanciate resource
        yield return new WaitForSeconds(2f);
    }
}
