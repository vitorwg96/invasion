using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TowerLife : PlaceableLife
{
    public event UnityAction TowerDie;


    public override void OnDied()
    {
        base.OnDied();
        Destroy(gameObject);
        TowerDie?.Invoke();
        //animator
        //sound
        //particle
    }
}
