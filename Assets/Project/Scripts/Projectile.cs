using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private int _damage;
    [SerializeField] private int _speed;

    private Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();

    }

    public void Shoot()
    {
        _rigidbody.velocity = Vector3.right * _speed;

    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out EnemyLife enemy))
        {
            enemy.TakeDamage(_damage);
            Destroy(gameObject);
        }
    }

}
