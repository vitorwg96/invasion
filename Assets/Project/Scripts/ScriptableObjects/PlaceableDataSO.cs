using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewPlaceable", menuName = "Invasion/Placeable Data")]
public class PlaceableDataSO : ScriptableObject
{
    [Header("Common")]
    //public Placeable.PlaceableType pType;
    [SerializeField] private GameObject _associatedPrefab;
    [SerializeField] private GameObject _objectPrefab; // can instanciate the projectile and resource (bad name)
    [SerializeField] private PlaceabldeState.States _initialState;

    [Header("Placeable Data")]
    //public ThinkingPlaceable.AttackType attackType = ThinkingPlaceable.AttackType.Melee;
    //public Placeable.PlaceableTarget targetType = Placeable.PlaceableTarget.Both;
    [SerializeField] private GameObject _shotterPoint;
    [SerializeField] private int _health;
    [SerializeField] private int _damage;
    [SerializeField] private float _mecanicCooldown;
    [SerializeField] private float _moveSpeed;

    [SerializeField] private AudioClip dieClip;


    public int Health => _health;

    public int Damage => _damage;
    public float MoveSpeed => _moveSpeed;

    public GameObject ObjectPrefab => _objectPrefab;

    public GameObject AssociatedPrefab => _associatedPrefab;

    public Transform ShotterPoint => _shotterPoint.transform;

    public float MecanicCooldown => _mecanicCooldown;

}
