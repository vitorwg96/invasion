using UnityEngine;

[CreateAssetMenu(fileName = "NewCard", menuName = "Invasion/Card Data")]
public class CardDataSO : ScriptableObject
{
    public enum CardType
    {
        Turrent = 0,
        Energy = 1,
        Tank = 2,
    }

    public int energyCost;
    public CardType type;
    public PlaceableDataSO placeablesData;
    

}