using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] private PlaceableDataSO _placeableData;

    private Vector3 _offSet = new Vector3(0f, 0.5f, 0f);
    private float _cd;

    private void Start()
    {
        StartCoroutine(LookingForTarget());
    }

    private IEnumerator LookingForTarget()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + _offSet, Vector3.right, out hit, 15))
        {
            //Debug.Log("Trying Shoot: " + hit.collider.gameObject.name);
            if (hit.collider.gameObject.TryGetComponent(out Movement _enemy))
            {
                    Shooting();
                    yield return new WaitForSeconds(_placeableData.MecanicCooldown);
                    StartCoroutine(LookingForTarget());
            }
        }
        else
        {
            yield return new WaitForSeconds(1f);
            LookingForTarget();
        }
    }

    private void Shooting()
    {
        GameObject projectile = Instantiate(_placeableData.ObjectPrefab, gameObject.transform.position + _offSet, Quaternion.identity);
        projectile.TryGetComponent(out Projectile p);
        p.Shoot();
    }
}
